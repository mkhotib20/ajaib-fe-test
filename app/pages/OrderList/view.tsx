import { RANDOM_USER_URL } from '@/constant';
import { message, Table } from 'antd';
import axios from 'axios';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { generateTableColumn } from './columns';
import Filters from './components/Filters';
import { FilterType, OnTableChangeOption, PaginationType, RandomUserApiResult, User } from './interface';

const ViewOrderList = () => {
  const [data, setData] = useState<User[]>([]);
  const [loading, setLoading] = useState(true);
  const [pagination, setPagination] = useState<PaginationType>({
    current: 1,
    pageSize: 10,
    total: 10,
  });

  const [order, setOrder] = useState<Record<string, 'ascend' | 'descend'>>({});
  const [filter, setFilter] = useState<FilterType>({});

  const { current, pageSize } = pagination;

  const requestParam = useMemo(() => {
    const orderKey = Object.keys(order)?.[0];
    return {
      ...filter,
      ...(order?.[orderKey] && {
        order: `${orderKey},${order[orderKey]}`,
      }),
      results: pageSize,
      page: current,
    };
  }, [current, pageSize, order, filter]);

  const fetchData = useCallback(async () => {
    try {
      if (!loading) setLoading(true);
      const searchParam = new URLSearchParams();
      Object.keys(requestParam).forEach((key) => {
        searchParam.append(key, requestParam[key]);
      });

      const response = await axios.get<RandomUserApiResult>(`${RANDOM_USER_URL}?${searchParam}`);
      const { results, info } = response.data;
      const { results: totalData } = info;
      setData(results);
      setPagination((prevPagination) => ({
        ...prevPagination,
        total: totalData,
      }));
    } catch (error) {
      console.log(error);
      message.warn('Something went wrong, please try again later');
    } finally {
      setLoading(false);
    }
  }, [requestParam]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const handleChangeSorter = (option: OnTableChangeOption) => {
    const { sorter } = option;
    const { field, columnKey, order } = sorter;
    setOrder({
      [(columnKey || field).toString()]: order,
    });
  };

  return (
    <>
      <Filters {...{ setFilter, filter, loading }} />
      <Table
        dataSource={data}
        loading={loading}
        columns={generateTableColumn({ order })}
        onChange={(pagination, _, sorter) =>
          handleChangeSorter({ ...pagination, sorter: Array.isArray(sorter) ? sorter[0] : sorter })
        }
        pagination={{
          ...pagination,
          // mock total, so that we can use table next page
          total: pagination.total * 2,
          onChange: (page, pageSize) =>
            setPagination((prevPagination) => ({
              ...prevPagination,
              current: page,
              pageSize,
            })),
          showSizeChanger: true,
        }}
      />
    </>
  );
};

export default ViewOrderList;
