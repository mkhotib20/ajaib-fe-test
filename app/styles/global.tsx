import { css, Global } from '@emotion/react';

export const globalStyle = (
  <Global
    styles={css`
      body,
      html {
        padding: 0;
        margin: 0;
      }
      .ant-message-notice {
        .ant-message-notice-content {
          border-radius: 8px;
          padding: 0;
          min-width: 300px;
          .ant-message-custom-content {
            text-align: left;
            padding: 10px 16px;
          }
          .ant-message-success {
          }
        }
      }
      * {
        font-family: 'Poppins', sans-serif;
      }

      .ant-form-item-label {
        label {
          font-weight: 600;
        }
      }
      label.ant-checkbox-wrapper {
        span {
          color: #999999;
        }
      }
      a {
        color: #5199dc;
        text-decoration: unset;
      }
      h3 {
        color: #1a2c38;
        font-size: 24px;
      }
      .ant-progress-success-bg,
      .ant-menu-horizontal:not(.ant-menu-dark) > .ant-menu-item:hover::after,
      .ant-menu-horizontal:not(.ant-menu-dark) > .ant-menu-submenu:hover::after,
      .ant-menu-horizontal:not(.ant-menu-dark) > .ant-menu-item-active::after,
      .ant-menu-horizontal:not(.ant-menu-dark) > .ant-menu-submenu-active::after,
      .ant-menu-horizontal:not(.ant-menu-dark) > .ant-menu-item-open::after,
      .ant-menu-horizontal:not(.ant-menu-dark) > .ant-menu-submenu-open::after,
      .ant-menu-horizontal:not(.ant-menu-dark) > .ant-menu-item-selected::after,
      .ant-menu-horizontal:not(.ant-menu-dark) > .ant-menu-submenu-selected::after {
        border-bottom: none;
        border: none;
      }
      .ant-menu-horizontal > .ant-menu-item::after,
      .ant-menu-horizontal > .ant-menu-submenu::after {
        border: none;
      }
    `}
  />
);
