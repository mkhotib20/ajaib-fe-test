import { useRef, useState, useEffect } from 'react';
import type { Dispatch, SetStateAction } from 'react';

interface useDebouceStateOptionType<T> {
  delay?: number;
  callback?: (value: T) => void;
}

const useDebouceState = <T>(
  initialState?: T,
  options?: useDebouceStateOptionType<T>
): [T, Dispatch<SetStateAction<T>>, boolean] => {
  const { delay = 1000, callback } = options || {};
  const [value, setValue] = useState<T>(initialState as T);

  const previousValue = useRef(value);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (previousValue.current !== value) {
      setLoading(true);
    }

    // cancel loading if user clear input before reaching timeout
    if (!value && !previousValue.current) {
      setLoading(false);
    }

    const handler = setTimeout(() => {
      if (previousValue.current !== value) {
        previousValue.current = value;
        if (callback) {
          callback(value);
        }
        setLoading(false);
      }
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [value, delay, callback]);

  return [value, setValue, loading];
};

export default useDebouceState;
