import dynamic from 'next/dynamic';

const Layout = dynamic(() => import('./view'));

export default Layout;
