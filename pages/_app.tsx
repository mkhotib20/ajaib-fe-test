import 'antd/dist/antd.css';
import Head from 'next/head';
import { globalStyle } from '@/styles/global';
import type { AppProps } from 'next/app';
import Layout from '@/components/Layout';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      {globalStyle}
      <Head>
        <title>Test Ajaib</title>
      </Head>

      <Layout>
        <Component {...pageProps} />
      </Layout>
    </>
  );
}

export default MyApp;
