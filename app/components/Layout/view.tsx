import { UserOutlined } from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import type { PropsWithChildren } from 'react';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

const ViewLayour = ({ children }: PropsWithChildren<{}>) => {
  return (
    <Layout>
      <Header css={{ display: 'flex' }}>
        <a href="/">Icon</a>
      </Header>
      <Layout>
        <Sider width={200} className="site-layout-background">
          <Menu
            mode="inline"
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            css={{ height: '100%', borderRight: 0 }}
          >
            <SubMenu key="sub1" icon={<UserOutlined />} title="Orders">
              <Menu.Item key="1">List</Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>

        <Layout css={{ padding: '0 24px 24px' }}>
          <Content
            css={{
              padding: 24,
              margin: 0,
              minHeight: 'calc(100vh - 24px - 64px)',
            }}
          >
            {children}
          </Content>
        </Layout>
      </Layout>
    </Layout>
  );
};

export default ViewLayour;
