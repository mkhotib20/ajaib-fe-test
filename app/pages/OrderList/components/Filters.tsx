import type { VFC, Dispatch, SetStateAction } from 'react';
import { Col, Row, Form, Select, Input, Button } from 'antd';
import { FilterType } from '../interface';
import useDebouceState from '@/hooks/useDebouceState';

const GENDERS = ['all', 'male', 'female'];

interface FilterProps {
  filter: FilterType;
  loading?: boolean;
  setFilter: Dispatch<SetStateAction<FilterType>>;
}

const Filters: VFC<FilterProps> = ({ filter, setFilter, loading }) => {
  const [searchValue, setSearchValue, typing] = useDebouceState('', {
    delay: 2000,
    callback: (search) => setFilter((prev) => ({ ...prev, search })),
  });

  const [form] = Form.useForm()
  const handleResetFilter = () => {
    form.resetFields()
    setFilter({})
  }

  return (
    <Form form={form} layout="vertical">
      <Row gutter={20} >
        <Col md={4} xs={12}>
          <Form.Item label="Search" name="search">
            <Input.Search
              disabled={loading}
              loading={typing || loading}
              value={searchValue}
              onChange={(event) => setSearchValue(event.currentTarget.value)}
            />
          </Form.Item>
        </Col>
        <Col md={4} xs={12}>
          <Form.Item label="Gender" name="gender">
            <Select
              // Prevent user press select while fetching data
              disabled={loading}
              onChange={(value) =>
                setFilter(({ /* remove gener filter first */ gender, ...prevFilter }) => ({
                  ...prevFilter,
                  ...(value !== 'all' && {
                    gender: value,
                  }),
                }))
              }
              defaultValue="all"
              options={GENDERS.map((gender) => ({ value: gender, label: gender.toUpperCase() }))}
            />
          </Form.Item>
        </Col>
        <Col md={4} xs={12}>
          <Button loading={loading} css={{marginTop: 30}} onClick={handleResetFilter}>Reset Filter</Button>
        </Col>
      </Row>
    </Form>
  );
};

export default Filters;
