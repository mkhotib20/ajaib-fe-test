import { Avatar, Space, Typography } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { User } from './interface';

const noopSorter = () => 0;

export const generateTableColumn = ({ order }): ColumnsType<User> => [
  {
    title: 'Name',
    dataIndex: ['name', 'first'],
    key: 'name',
    sortOrder: order['name'],
    render: (value, record) => (
      <Space>
        <Avatar src={record.picture.thumbnail} />
        <Typography.Text ellipsis>{value}</Typography.Text>
      </Space>
    ),
    sorter: noopSorter,
  },
  {
    title: 'Emails',
    dataIndex: 'email',
    sortOrder: order['email'],
    sorter: noopSorter,
  },
  {
    title: 'Age',
    dataIndex: ['dob', 'age'],
    key: 'age',
    sortOrder: order['age'],
    sorter: noopSorter,
  },
  {
    title: 'Gender',
    dataIndex: 'gender',
    render: (value) => value.toUpperCase(),
    sorter: noopSorter,
    sortOrder: order['gender'],
  },
  {
    title: 'Country',
    key: 'country',
    sortOrder: order['country'],
    dataIndex: ['location', 'country'],
    sorter: noopSorter,
  },
];
