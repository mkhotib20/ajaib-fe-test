import dynamic from 'next/dynamic';

const OrderList = dynamic(() => import('./view'));

export default OrderList;
