## USER LIST Randomuser.me &middot; [![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://github.com/some-package/blob/master/LICENSE)

#### This project was bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

### This project is used to display a list of users from randomuser.me. The project has the following functions : 

- **Search by name :** You can search user by their name
- **Data size changer :** You can change your data amount to be shown on each page
- **Filter by gender :** You can filter user by their gender
- **Sort by everything on the column :** Yes, you can sort all column ascending or descending

### How to run
#### <strong> please take a note, this project crafted with pnpm, so the module version locked with pnpm-lock.yml. To prevent unexpected error because different library version you've installed, i recommend to use pnpm or import the lock file to your prefered package manager </strong>

```console
git clone https://gitlab.com/mkhotib20/ajaib-fe-test.git

pnpm run install

```

### Contributing

Feel free to contribute to our repo. Please fork this repo and create your PR, I might consider your contribution
